defmodule Kuber.Billing do

  alias Commerce.Billing
  alias Billing.{CreditCard, Address, Worker, Gateways}

  @config %{credentials: {"sk_test_9uOalZ9gfBE8GannEgkMCAqN", ""}, default_currency: "USD"}

  @card %CreditCard{
    name: "Sagar Karwande",
    number: "4242424242424242",
    expiration: {2018, 12},
    cvc:  "123"
  }

  @address %Address{
    street1: "123 Main",
    street2: "Suite 100",
    city: "New York",
    region: "NY",
    country: "US",
    postal_code: "11111"
  }

  def authorize() do
    case Billing.purchase(:stripe_gateway, 200, @card, billing_address: @address, description: "Amazing T-Shirt") do
        {:ok,    %{authorization: authorization}} ->
          IO.puts("Payment authorized #{authorization}")

        {:error, %{code: :declined, reason: reason}} ->
          IO.puts("Payment declined #{reason}")

        {:error, error} ->
          IO.inspect error

    end
  end

end
